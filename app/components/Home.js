// @flow
import React, { Component } from 'react';
import { Link } from 'react-router';


export default class Home extends Component {
  render() {
    return (
      <div>
        <header className="ph5 pt4 base">
          <h2 className="f1">
            <Link style={{ textDecoration: 'none' }} to="/"><span className="base">Skool</span></Link>
          </h2>
        </header>
        <nav className="w100 bt bb bw1 bg-yellow1">
          <div className="dib f6 ma3 pv2 ph3 ba bw2 link gray9 pointer dim bg-white">
            <Link style={{ textDecoration: 'none' }} to="/counter"><span className="indigo9">Counter</span></Link>
          </div>
          <div className="dib f6 ma3 pv2 ph3 ba bw2 link gray9 pointer dim bg-white">
            <Link style={{ textDecoration: 'none' }} to="/templates"><span className="indigo9">Templates</span></Link>
          </div>
        </nav>
      </div>
    );
  }
}

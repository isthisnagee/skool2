// @flow
import React, { Component } from 'react';
import { Link } from 'react-router';

// TODO: hoist nav, header to classes
class Template extends Component {
  render() {
    return (
      <div>
        <header className="ph5 pt4 base">
          <h2 className="f1">
            <Link style={{ textDecoration: 'none' }} to="/">
              <span className="base">Skool</span>
            </Link>
          </h2>
        </header>
      </div>
    );
  }
}

export default Template;

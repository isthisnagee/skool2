// @flow
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Template from '../components/Template';
import * as TemplateActions from '../actions/template';

function mapStateToProps(state) {
  return {
    name: state.name
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(TemplateActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Template);

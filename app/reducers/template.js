import { ADD_TEMPLATE,
         DELETE_TEMPLATE,
         RECOVER_TEMPLATE } from '../actions/template';

export default function template(state: string = '', action: Object) {
  switch (action.type) {
    case ADD_TEMPLATE:
      return 'add template';
    case DELETE_TEMPLATE:
      return 'delete template';
    case RECOVER_TEMPLATE:
      return 'recover template';
    default:
      return 'nope';
  }
}

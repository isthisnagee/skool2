// @flow
export const ADD_TEMPLATE = 'ADD_TEMPLATE';
export const DELETE_TEMPLATE = 'DELETE_TEMPLATE';
export const RECOVER_TEMPLATE = 'RECOVER_TEMPLATE';

/*
 * TODO: addTemplate
 * Adds a template from an editor box?
 */
export function addTemplate() {
  return {
    type: ADD_TEMPLATE
  };
}

/*
 * TODO: deleteTemplate
 * should delete the given template and puts it
 * in a temporary buffer, giving the user a chance to
 * recover. Maybe calls a maybeRecoverTemplate method
 */
export function deleteTemplate() {
  return {
    type: DELETE_TEMPLATE
  };
}

/*
 * TODO: reocverTemplate
 * should recover a template put in a temp buffer and
 * clear that buffer
 */
export function recoverTemplate() {
  return {
    type: RECOVER_TEMPLATE
  };
}

